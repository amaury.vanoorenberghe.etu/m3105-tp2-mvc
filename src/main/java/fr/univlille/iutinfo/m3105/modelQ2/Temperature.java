package fr.univlille.iutinfo.m3105.modelQ2;

import fr.univlille.iutinfo.m3105.modelQ1.ITemperature;
import fr.univlille.iutinfo.m3105.utils.ConnectableProperty;

public class Temperature extends ConnectableProperty implements ITemperature {
	private final Echelle ECHELLE;
	
	public Temperature(Echelle echelle) {
		ECHELLE = echelle;
		setValue(0.0);
	}

	public Echelle getEchelle() {
		return ECHELLE;
	}

	@Override
	public void setTemperature(double d) {
		setValue(ECHELLE.toKelvin(d));
	}

	@Override
	public Double getTemperature() {
		return ECHELLE.fromKelvin((Double)getValue());
	}

	@Override
	public void incrementTemperature() {
		addToTemperature(+1.0);
	}

	@Override
	public void decrementTemperature() {
		addToTemperature(-1.0);
	}
	
	private void addToTemperature(Double x) {
		setTemperature(getTemperature() + x);
	}

	@Override
	public ConnectableProperty temperatureProperty() {
		return this;
	}
}
