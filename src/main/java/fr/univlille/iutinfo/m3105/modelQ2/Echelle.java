package fr.univlille.iutinfo.m3105.modelQ2;

import java.util.function.Function;

public enum Echelle {
	//DELISLE	("Delisle", 	"°De"),
	//LEYDEN	("Leyden", 		"°Le"),
	//ROMER		("Røner", 		"°Rø"),
	KELVIN		("Kelvin", 		"K", 	Conversions::kelvinToKelvin, 		Conversions::kelvinToKelvin),
	CELSIUS		("Celsius", 	"C", 	Conversions::kelvinToCelsius, 		Conversions::celsiusToKelvin), 
	NEWTON		("Newton", 		"N",	Conversions::kelvinToNewton, 		Conversions::newtonToKelvin),
	FAHRENHEIT	("Fahrenheit", 	"F",	Conversions::kelvinToFahrenheit, 	Conversions::fahrenheitToKelvin),
	RANKINE		("Rankine", 	"Ra", 	Conversions::kelvinToRankine,		Conversions::rankineToKelvin),
	REAUMUR		("Réaumur", 	"Ré",	Conversions::kelvinToReaumur,		Conversions::reaumurToKelvin);

	private final String NAME;
	private final String ABBREVIATION;
	private final Function<Double, Double> FROM_KELVIN;
	private final Function<Double, Double> TO_KELVIN;
	
	private Echelle(String name, String abbreviation, Function<Double, Double> fromKelvin, Function<Double, Double> toKelvin) {
		NAME = name;
		ABBREVIATION = abbreviation;
		FROM_KELVIN = fromKelvin;
		TO_KELVIN = toKelvin;
	}
	
	public String getName() {
		return NAME;
	}

	public String getAbbrev() {
		return ABBREVIATION;
	}

	public double fromKelvin(double d) {
		return FROM_KELVIN.apply(d);
	}

	public double toKelvin(double d) {
		return TO_KELVIN.apply(d);
	}

}
